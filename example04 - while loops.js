"use strict";

var sum = 0;
var i = 2;

// While, i is less than 5...
while (i < 5) {

    // Do this stuff.
    sum += i;
    console.log(i);

    // Since we're incrementing i before we loop around, i will eventually
    // not be less than 5, and the loop will exit.
    i++;
}

console.log(sum);

// For the daring: Uncomment and run these lines.
// What happens? Why?

// var  j = 0;
// while (j >= 0) {
//     j++;
//     console.log(j);
// }