"use strict";

/* NOTE: "use strict" should be at the beginning of every JS file.
   It tells the JS interpreter to be "strict" about the syntax, and will
   help you avoid making dumb mistakes (the program will stop working rather
   than happily continue and give weird results). */

// Creating some variables
var myFirstVar = 3;
var x = 1;
var y = 2;

// We can log anything to the console, to see its value for
// debugging purposes.
console.log(myFirstVar);

// We can set new values for existing variables easily
y = 5;

// We can use arithmetic operators
var z = x + y;

// ++ will add one. -- will subtract one.
x++;
y--;

// +=, -=, *=, /= are shorthands...
x -= 3 // Shorthand for x = x - 3

// We can define Strings (which is basically text).
var h = "Hello";
var w = "World";

// "Adding" strings together concatenates them.
var helloWorld = h + " " + w; // "Hello World"

// Adding Strings and numbers also concatenates them.
helloWorld += 123;
console.log(helloWorld); // Will print "Hello World123"

// Can convert strings to numbers, and vice versa.
var num = 4;
var str = "" + 4; // Concatenate a number to the end of an empty string.
                  // Result will be the string "4".

// parseInt will convert a string into an integer (no decimal point).
var fourtyTwo = "42";
var numberFourtyTwo = parseInt(fourtyTwo);

// parseFloat will convert a string into a floating-point (decimal) number.
var pi = "3.14159";
var numberPi = parseFloat(pi);